package pl.panmalina.springsecurity.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.panmalina.springsecurity.repositories.UserRepository;

import javax.transaction.Transactional;

@Component
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DataInserter dataInserter;

    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initUsers();
    }

    private void initUsers() {
//        dataInserter.initUsers();
    }
}
