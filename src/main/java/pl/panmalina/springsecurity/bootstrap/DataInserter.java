package pl.panmalina.springsecurity.bootstrap;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.panmalina.springsecurity.model.Role;
import pl.panmalina.springsecurity.model.User;
import pl.panmalina.springsecurity.repositories.UserRepository;

@Service
@Log4j2
public class DataInserter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    public void init() {
        initUsers();
    }

    private void initUsers() {
        log.info("Init users");
        User user1 = User.builder()
                .username("user1")
                .password(passwordEncoder.encode("user1"))
                .role(Role.USER)
                .build();

        User admin1 = User.builder()
                .username("admin1")
                .password(passwordEncoder.encode("admin1"))
                .role(Role.ADMIN)
                .build();

        userRepository.save(user1);
        userRepository.save(admin1);
    }
}