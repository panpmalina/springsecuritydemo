package pl.panmalina.springsecurity.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.panmalina.springsecurity.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
