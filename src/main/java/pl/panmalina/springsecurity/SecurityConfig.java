package pl.panmalina.springsecurity;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import pl.panmalina.springsecurity.bootstrap.DataInserter;
import pl.panmalina.springsecurity.model.User;
import pl.panmalina.springsecurity.repositories.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Configuration
@Log4j2
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DataInserter dataInserter;


    @Bean
    public UserDetailsService userDetailsService() {
        log.info("Number of users " + userRepository.count());

        Iterable<pl.panmalina.springsecurity.model.User> users = userRepository.findAll();
        UserDetails[] userDetails = StreamSupport.stream(users.spliterator(), false)
                .map(User::convertToUserDetails)
                .toArray(size -> new UserDetails[size]);

        log.info("Add users to memory");
        return new InMemoryUserDetailsManager(userDetails);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/h2-console").permitAll()
                .antMatchers(HttpMethod.GET, "/citation").permitAll()
                .antMatchers(HttpMethod.POST, "/citations").hasRole("MODERATOR")
                .antMatchers(HttpMethod.DELETE, "/citations").hasRole("ADMIN")
                .and().formLogin().permitAll().defaultSuccessUrl("/citations")
                .and().csrf().disable()
                .headers().frameOptions().disable();
    }

    @PostConstruct
    private void init() {
        dataInserter.init();
    }
}
