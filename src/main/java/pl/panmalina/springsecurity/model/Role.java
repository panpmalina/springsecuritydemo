package pl.panmalina.springsecurity.model;

public enum Role {
    USER,
    MODERATOR,
    ADMIN
}
