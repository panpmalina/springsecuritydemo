package pl.panmalina.springsecurity.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.*;
import pl.panmalina.springsecurity.model.Citation;

import java.util.ArrayList;
import java.util.List;

@RestController
@Log4j2
public class CitationsController {

    List<Citation> citations = new ArrayList<>(3);

    public CitationsController() {
        citations.add(new Citation("To, że milczę, nie znaczy, że nie mam nic do powiedzenia.", "Jonathan Carroll"));
        citations.add(new Citation("Lepiej zaliczać się do niektórych, niż do wszystkich.", "Andrzej Sapkowski"));
        citations.add(new Citation("Czytanie książek to najpiękniejsza zabawa, jaką sobie ludzkość wymyśliła", "Wisława Szymborska"));
    }

    @GetMapping("/citations")
    public List<Citation> getCitations() {
        return citations;
    }

    @PostMapping("/citations")
    public void addCitation(@RequestBody Citation citation) {
        citations.add(citation);
    }

    @DeleteMapping("/citations/{index}")
    public void deleteCitation(@PathVariable("index") int index){
        citations.remove(index);
    }

    /*
    Only for web browsers
     */
    private boolean isLogged() {
        Object detailsObj = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (detailsObj instanceof WebAuthenticationDetails) {
            WebAuthenticationDetails details = (WebAuthenticationDetails) detailsObj;
            return details.getSessionId() != null;
        } else
            return false;
    }
}
